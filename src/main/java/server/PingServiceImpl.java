package server;

import cz.cvut.fel.dsv.PingServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.stub.StreamObserver;
import node.NodeStructure;

public class PingServiceImpl extends PingServiceGrpc.PingServiceImplBase {
    @Override
    public void ping(Services.PingRequest request, StreamObserver<Services.PingResponse> responseObserver) {
        Services.PingResponse response = Services.PingResponse.newBuilder()
                .setIsAlive(true)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void print(Services.Empty request, StreamObserver<Services.PrintResponse> responseObserver) {
        Services.PrintResponse response = Services.PrintResponse.newBuilder()
                .addAllDataArray(NodeStructure.getSharedVariable())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void startGenerator(Services.Empty request, StreamObserver<Services.Empty> responseObserver) {
        Services.Empty empty = Services.Empty.newBuilder().build();

        ServerApp.generator.start();

        responseObserver.onNext(empty);
        responseObserver.onCompleted();
    }

    @Override
    public void setDelay(Services.SetDelayRequest request, StreamObserver<Services.Empty> responseObserver) {
        if (request.getDelayMilliseconds() >= 0) {
            NodeStructure.setDelayMilliseconds(request.getDelayMilliseconds());
        } else {
            try {
                throw new Exception("Negative delay");
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }

        Services.Empty empty = Services.Empty.newBuilder().build();

        responseObserver.onNext(empty);
        responseObserver.onCompleted();
    }

    @Override
    public void stopGenerator(Services.Empty request, StreamObserver<Services.Empty> responseObserver) {
        Services.Empty empty = Services.Empty.newBuilder().build();

        ServerApp.generator.stop();

        responseObserver.onNext(empty);
        responseObserver.onCompleted();
    }
}
