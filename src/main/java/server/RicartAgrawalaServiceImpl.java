package server;

import cz.cvut.fel.dsv.RicartAgrawalaServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.stub.StreamObserver;
import node.LogicalClock;
import node.NodeStructure;
import utils.Utils;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

public class RicartAgrawalaServiceImpl extends RicartAgrawalaServiceGrpc.RicartAgrawalaServiceImplBase {
    private static final Logger LOGGER = Logger.getLogger(RicartAgrawalaServiceImpl.class.getName());

    @Override
    public void requestAccess(Services.RequestAccessRequest request,
                              StreamObserver<Services.RequestAccessResponse> responseObserver) {
        var prevTick = LogicalClock.getMyRq();
        var tick = LogicalClock.getMaxRq(request.getTick());
        var requestQueue = NodeStructure.getRequestQueue();
        var first = requestQueue.getFirst();

        if (NodeStructure.getDelayMilliseconds() > 0) {
            try {
                Thread.sleep(NodeStructure.getDelayMilliseconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // waiting for previous request if it exists
        if (first.isPresent()) {
            // [L] Delay := Req[i] and ((k>MyRq) or (k=MyRq and j>i)); [U]
            if ((request.getTick() > prevTick) || request.getTick() == prevTick && request.getPort() > Utils.getPort(NodeStructure.getMyAddress())) {
                try {
                    LOGGER.info("REPLY " + NodeStructure.getMyAddress() + " " + tick + " waiting for timestamp " + first.get().getTick());
                    first.get().lock();
                    LOGGER.info("REPLY " + NodeStructure.getMyAddress() + " " + tick + " waited for timestamp " + first.get().getTick());
                } finally {
                    first.get().unlock();
                }

                LOGGER.info(
                        "\nREQUEST INFO request.getTick(): " + request.getTick() +
                                "prevTick: " + prevTick +
                                "request.getPort(): " + request.getPort() +
                                "Utils.getPort(NodeStructure.getMyAddress()): " + Utils.getPort(NodeStructure.getMyAddress()) + "\n"
                );
            } else {
                LOGGER.info("REPLY for " + tick + " because of PRIORITY");

                LOGGER.info(
                        "request.getTick(): " + request.getTick() +
                                "\nprevTick: " + prevTick +
                                "\nrequest.getPort(): " + request.getPort() +
                                "\nUtils.getPort(NodeStructure.getMyAddress()): " + Utils.getPort(NodeStructure.getMyAddress())
                );

                Services.RequestAccessResponse response;
                response = Services.RequestAccessResponse.newBuilder()
                        .setTick(tick)
                        .setAccessIsGiven(true)
                        .build();

                responseObserver.onNext(response);
                responseObserver.onCompleted();
                return;
            }
        } else {
            LOGGER.info("REPLY " + NodeStructure.getMyAddress() + " " + tick + " waited for no-one");
        }

        LOGGER.info("REPLY for " + tick);

        Services.RequestAccessResponse response;
        response = Services.RequestAccessResponse.newBuilder()
                .setTick(tick)
                .setAccessIsGiven(true)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void syncData(Services.SyncDataRequest request,
                         StreamObserver<Services.SyncDataResponse> responseObserver) {

        LOGGER.info(
                "I [" +
                        NodeStructure.getMyAddress() +
                        "] am getting data to synchronize on index ["
                        + request.getIndex()
                        + "] with data <"
                        + request.getPayload()
                        + "> "
                        + LogicalClock.stringOfCurrent()
        );
        NodeStructure.getSharedVariable().set(request.getIndex(), request.getPayload());
        var tick = LogicalClock.getMyRqInc();
        LOGGER.info(
                "Shared variable AFTER synchronizing: " + LogicalClock.stringOfCurrent() + " "
                        + NodeStructure.getSharedVariable().toString()
        );

        Services.SyncDataResponse response =
                Services.SyncDataResponse.newBuilder()
                        .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void syncAllData(Services.SyncAllDataRequest request,
                            StreamObserver<Services.SyncAllDataResponse> responseObserver) {
        List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> stubs = NodeStructure.createRicartAgrawalaStubs();

        for (var stub : stubs) {
            Services.SetNewNodeRequest setNewNodeRequest = Services.SetNewNodeRequest.newBuilder().setAddress(request.getMyAddress()).build();
            Services.SetNewNodeResponse setNewNodeResponse = stub.setNewNode(setNewNodeRequest);
        }

        CopyOnWriteArrayList<String> neighbours = new CopyOnWriteArrayList<>(NodeStructure.getNeighbours());
        neighbours.add(NodeStructure.getMyAddress());

        Services.SyncAllDataResponse response =
                Services.SyncAllDataResponse.newBuilder()
                        .addAllDataArray(NodeStructure.getSharedVariable())
                        .addAllNodesArray(neighbours)
                        .build();

        NodeStructure.getNeighbours().addIfAbsent(request.getMyAddress());

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void setNewNode(Services.SetNewNodeRequest request,
                           StreamObserver<Services.SetNewNodeResponse> responseObserver) {
        NodeStructure.getNeighbours().addIfAbsent(request.getAddress());

        Services.SetNewNodeResponse response = Services.SetNewNodeResponse.newBuilder().build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
