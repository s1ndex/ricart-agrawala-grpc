package server;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;
import cz.cvut.fel.dsv.RicartAgrawalaServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import node.Blocker;
import node.LogicalClock;
import node.NodeStructure;
import node.RequestQueue;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class ArrayIntServiceImpl extends ArrayIntServiceGrpc.ArrayIntServiceImplBase {
    private static final Logger LOGGER = Logger.getLogger(ArrayIntServiceImpl.class.getName());

    @Override
    public void getData(Services.GetDataRequest request, StreamObserver<Services.GetDataResponse> responseObserver) {

        int index = request.getIndex();
        int data = NodeStructure.getSharedVariable().get(index);

        LOGGER.info(
                "Incoming request to get data from index: ["
                        + index
                        + "], where data is <"
                        + data
                        + "> "
                        + LogicalClock.stringOfCurrent()
        );
        Services.GetDataResponse dataResponse =
                Services.GetDataResponse.newBuilder()
                        .setPayload(data)
                        .build();

        responseObserver.onNext(dataResponse);
        responseObserver.onCompleted();

    }

    @Override
    public void setData(Services.SetDataRequest request, StreamObserver<Services.SetDataResponse> responseObserver) {
        synchronized (NodeStructure.getSynchronizer()) {
            Blocker blocker = null;
            RequestQueue requestQueue = null;
            try {
                Optional<Blocker> prev;
                long tick;

                tick = LogicalClock.getMyRqInc();
                blocker = new Blocker(tick);
                requestQueue = NodeStructure.getRequestQueue();

                blocker.lock();
                LOGGER.info(
                        "I ["
                                + NodeStructure.getMyAddress()
                                + "] want to SET data at time: "
                                + tick
                );

                prev = requestQueue.pushAndGetPrevious(blocker);

                //wait previous request on the same node
                if (prev.isPresent()) {
                    try {
                        LOGGER.info("SET " + NodeStructure.getMyAddress() + " " + tick + " waiting for " + prev.get().getTick());
                        prev.get().lock();
                        LOGGER.info("SET " + NodeStructure.getMyAddress() + " " + tick + " waited for " + prev.get().getTick());
                    } finally {
                        prev.get().unlock();
                    }
                } else {
                    LOGGER.info("SET " + NodeStructure.getMyAddress() + " " + tick + " waited for no-one");
                }

                var ricartAgrawalaStubs = getReplies(tick);

                LOGGER.info(
                        "I ["
                                + NodeStructure.getMyAddress()
                                + "] have ENTERED critical section "
                                + LogicalClock.stringOfCurrent()
                );

                int index = request.getIndex();
                int data = request.getPayload();

                NodeStructure.getSharedVariable().set(index, data);

                syncAllNodes(ricartAgrawalaStubs, index, data);

                Services.SetDataResponse dataResponse =
                        Services.SetDataResponse.newBuilder().build();

                responseObserver.onNext(dataResponse);

                LOGGER.info(
                        "Shared variable AFTER changes: " + tick + " "
                                + NodeStructure.getSharedVariable().toString()
                );

                responseObserver.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.warning("EXCEPTION IN SET DATA");
                throw e;
            } finally {
                requestQueue.pop();
                blocker.unlock();
            }
        }
    }

    private List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> getReplies(long tick) {
        var channels = NodeStructure.createClientChannels();
        List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> ricartAgrawalaStubs = new ArrayList<>();

        for (var channel : channels) {
            var stub = channel.createRicartAgrawalaStub();
            ricartAgrawalaStubs.add(stub);

            try {
                Services.RequestAccessResponse response = stub.requestAccess(
                        Services.RequestAccessRequest.newBuilder()
                                .setTick(tick)
                                .setPort(Utils.getPort(NodeStructure.getMyAddress()))
                                .build()
                );

                LOGGER.info(
                        "RESPONSE to access shared variable: "
                                + response.getAccessIsGiven()
                                + " " + LogicalClock.stringOfCurrent()
                                + " FROM node " + channel.getAddress()
                );
            } catch (StatusRuntimeException ex) {
                //Second try. If there will be an exception again, we will go down.
                if (channel.CheckIsAlive()) {
                    Services.RequestAccessResponse response = stub.requestAccess(
                            Services.RequestAccessRequest.newBuilder()
                                    .setTick(tick)
                                    .setPort(Utils.getPort(NodeStructure.getMyAddress()))
                                    .build()
                    );

                    LOGGER.info(
                            "RESPONSE to access shared variable: "
                                    + response.getAccessIsGiven()
                                    + " " + LogicalClock.stringOfCurrent()
                                    + " FROM node " + channel.getAddress()
                    );
                    continue;
                }

                //Node is dead
                NodeStructure.getNeighbours().remove(channel.getAddress());
                LOGGER.info("Node: " + channel.getAddress() + " has been removed from neighbours " + LogicalClock.stringOfCurrent());
            }
        }

        return ricartAgrawalaStubs;
    }

    private void syncAllNodes(List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> ricartAgrawalaStubs, int index, int data) {
        LOGGER.info("Sending data to synchronize among all nodes " + LogicalClock.stringOfCurrent());
        for (RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub stub : ricartAgrawalaStubs) {
            // if someone died then remove it from neighbours and proceed to set the variable
            try {
                Services.SyncDataResponse response = stub.syncData(
                        Services.SyncDataRequest.newBuilder()
                                .setIndex(index)
                                .setPayload(data)
                                .build()
                );
            } catch (StatusRuntimeException ex) {
                //do nothing
            }
        }
    }
}
