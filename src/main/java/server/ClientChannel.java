package server;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;
import cz.cvut.fel.dsv.PingServiceGrpc;
import cz.cvut.fel.dsv.RicartAgrawalaServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class ClientChannel {
    private final String address;
    private final ManagedChannel channel;

    public ClientChannel(String address) {
        this.address = address;
        channel = ManagedChannelBuilder
                .forTarget(address)
                .usePlaintext()
                .build();
    }

    public String getAddress() {
        return address;
    }

    public RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub createRicartAgrawalaStub() {
        return RicartAgrawalaServiceGrpc.newBlockingStub(channel);
    }

    public ArrayIntServiceGrpc.ArrayIntServiceBlockingStub createArrayIntStub() {
        return ArrayIntServiceGrpc.newBlockingStub(channel);
    }

    public PingServiceGrpc.PingServiceBlockingStub createPingStub() {
        return PingServiceGrpc.newBlockingStub(channel);
    }

    public boolean CheckIsAlive() {
        var stub = createPingStub();
        final int maxTries = 3;
        final int delay = 1000;

        for (var i = 0; i < maxTries; i++) {
            try {
                var response = stub.ping(Services.PingRequest.newBuilder().build());
                return response.getIsAlive();
            } catch (StatusRuntimeException ex) {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }
}
