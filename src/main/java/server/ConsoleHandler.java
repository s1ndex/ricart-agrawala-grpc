package server;

import cz.cvut.fel.dsv.Services;
import node.NodeStructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class ConsoleHandler implements Runnable {
    private boolean reading = true;
    private final BufferedReader reader;
    private final PrintStream err = System.err;

    public ConsoleHandler() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    private void parse_commandline(String commandline) {
        if (commandline == null) {
            return;
        }

        if (commandline.contains("set")) {
            String[] args = commandline.split(" ");

            Services.SetDataRequest setRequest =
                    Services.SetDataRequest.newBuilder()
                            .setIndex(Integer.parseInt(args[1]))
                            .setPayload(Integer.parseInt(args[2]))
                            .build();
            Services.SetDataResponse dataResponse = NodeStructure.getMyArrayIntStub().setData(setRequest);
        } else if (commandline.contains("get")) {
            String[] args = commandline.split(" ");
            int index = Integer.parseInt(args[1]);

            Services.GetDataRequest getRequest =
                    Services.GetDataRequest.newBuilder()
                            .setIndex(index)
                            .build();
            Services.GetDataResponse dataResponse = NodeStructure.getMyArrayIntStub().getData(getRequest);
            System.out.println("INDEX: " + index + "; DATA: " + dataResponse.getPayload() + ";");
        } else if (commandline.equals("print")) {
            System.out.println("Shared variable: " + NodeStructure.getSharedVariable().toString());
        } else if (commandline.equals("start")) {
            ServerApp.generator.start();
        } else if (commandline.contains("delay")) {
            String[] args = commandline.split(" ");
            int mills = Integer.parseInt(args[1]);
            NodeStructure.setDelayMilliseconds(mills);
        } else if (commandline.equals("stop")) {
            ServerApp.generator.stop();
        } else if (commandline.equals("?") || commandline.equals("help")) {
            System.out.println("set INDEX DATA\t\tsets DATA on INDEX");
            System.out.println("get INDEX\t\t\tgets data from INDEX");
            System.out.println("print\t\t\t\tprints current data of shared variable");
            System.out.println("start\t\t\t\tstarts automatic mode");
            System.out.println("delay\t\t\t\tsets delay on reply");
            System.out.println("stop\t\t\t\tstops automatic mode");
        } else {
            System.out.println("Unrecognized command");
        }
    }

    @Override
    public void run() {
        String commandline = "";
        while (reading) {
            commandline = "";
            System.out.print("\ncmd > ");
            try {
                commandline = reader.readLine();
                parse_commandline(commandline);
            } catch (IOException e) {
                err.println("ConsoleHandler - error in reading console input.");
                e.printStackTrace();
                reading = false;
            }
        }
        System.out.println("Closing ConsoleHandler.");
    }
}