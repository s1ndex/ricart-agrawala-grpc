package server;

import cz.cvut.fel.dsv.RicartAgrawalaServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import node.LoadGenerator;
import node.NodeStructure;
import utils.LoggerInit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import static utils.Utils.getPort;

public class ServerApp {
    private static final Logger LOGGER = Logger.getLogger(ServerApp.class.getName());

    public static LoadGenerator generator;

    public static void main(String[] args) throws IOException, InterruptedException {
        LoggerInit.init();
        LOGGER.info("Server [" + args[0] + "] is started and ready!");

        String myAddress = args[0];

        CopyOnWriteArrayList<Integer> sharedVariable = new CopyOnWriteArrayList<>(new ArrayList<>(100));
        for (int i = 0; i < 100; i++) {
            sharedVariable.add(i, 0);
        }

        CopyOnWriteArrayList<String> neighbours = new CopyOnWriteArrayList<>();

        if (!args[0].equals(args[1])) {
            ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(args[1])
                    .usePlaintext()

                    .build();

            RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub blockingStub = RicartAgrawalaServiceGrpc.newBlockingStub(channel);
            Services.SyncAllDataRequest dataRequest = Services.SyncAllDataRequest.newBuilder()
                    .setMyAddress(myAddress)
                    .build();
            Services.SyncAllDataResponse dataResponse = blockingStub.syncAllData(dataRequest);

            neighbours.addAll(dataResponse.getNodesArrayList());
            sharedVariable = new CopyOnWriteArrayList<>(dataResponse.getDataArrayList());
        }

        NodeStructure.setNeighbours(neighbours);
        NodeStructure.setSharedVariable(sharedVariable);
        NodeStructure.setMyAddress(myAddress);
        int port = getPort(myAddress);

        if (neighbours.size() == 0) {
            System.out.println("This node is surrounded by : nothing so far");
        } else {
            System.out.println("This node is surrounded by : " + neighbours);
        }

        Server server = ServerBuilder
                .forPort(port)
                .addService(new ArrayIntServiceImpl())
                .addService(new RicartAgrawalaServiceImpl())
                .addService(new PingServiceImpl())
                .build();

        server.start();
        System.out.println("Server started on port: " + port);

        generator = new LoadGenerator();

//        ConsoleHandler consoleHandler = new ConsoleHandler();
//        new Thread(consoleHandler).start();

        server.awaitTermination();
        server.shutdownNow();
    }
}
