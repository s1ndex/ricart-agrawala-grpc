package node;

import java.util.Optional;
import java.util.TreeSet;
import java.util.logging.Logger;

public class RequestQueue {
    TreeSet<Blocker> set;

    private final Object synchronizer = new Object();
    private static final Logger LOGGER = Logger.getLogger(RequestQueue.class.getName());

    public RequestQueue() {
        set = new TreeSet<>();
    }

    public Optional<Blocker> pushAndGetPrevious(Blocker blocker) {
        synchronized (synchronizer) {
            LOGGER.info("Queue push " + blocker.getTick());
            set.add(blocker);

            Optional<Blocker> res = Optional.empty();
            Blocker prev = null;

            for (var b : set) {
                if (b == blocker) {
                    if (prev != null) {
                        res = Optional.of(prev);
                    }

                    break;
                }

                prev = b;
            }

            return res;
        }
    }

    public Optional<Blocker> getFirst() {
        synchronized (synchronizer) {
            return set.stream().findFirst();
        }
    }

    public void pop() {
        synchronized (synchronizer) {
            LOGGER.info("Queue pop " + set.first().getTick());
            if (!set.isEmpty()) {
                set.remove(set.first());
            }
        }
    }
}
