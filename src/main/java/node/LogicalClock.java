package node;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongBinaryOperator;

public class LogicalClock {
    private static final AtomicLong myRq = new AtomicLong(0);
    private static final AtomicLong maxRq = new AtomicLong(0);

    public static String stringOfCurrent() {
        return "at timestamp MyRq: " + getMyRq() + " MaxRq: " + maxRq.get();
    }

    public static long getMyRqInc() {
        long x = maxRq.get() + 1L;
        myRq.set(x);

        return x;
    }

    public static long getMyRq() {
        return myRq.get();
    }

    public static long getMaxRq(long r) {
        LongBinaryOperator binaryOperator = (x, y) -> Math.max(x, y) + 1L;

        return maxRq.accumulateAndGet(r, binaryOperator);
    }
}
