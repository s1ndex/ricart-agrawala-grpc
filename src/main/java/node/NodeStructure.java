package node;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;
import cz.cvut.fel.dsv.RicartAgrawalaServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import server.ClientChannel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class NodeStructure {
    private static String myAddress;
    private static int delayMilliseconds;

    private static CopyOnWriteArrayList<String> neighbours;
    private static CopyOnWriteArrayList<Integer> sharedVariable;
    private static final Object synchronizer = new Object();

    private static final RequestQueue requestQueue = new RequestQueue();

    public static Object getSynchronizer() {
        return synchronizer;
    }

    public static List<ClientChannel> createClientChannels() {
        return neighbours.stream().map(x -> new ClientChannel(x)).collect(Collectors.toList());
    }

    public static List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> createRicartAgrawalaStubs() {
        List<RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub> ricartAgrawalaStubs = new ArrayList<>();

        for (String neighbour : neighbours) {
            ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(neighbour)
                    .usePlaintext()
                    .build();

            RicartAgrawalaServiceGrpc.RicartAgrawalaServiceBlockingStub ricartAgrawalaStub =
                    RicartAgrawalaServiceGrpc.newBlockingStub(channel);

            ricartAgrawalaStubs.add(ricartAgrawalaStub);
        }

        return ricartAgrawalaStubs;
    }

    public static ArrayIntServiceGrpc.ArrayIntServiceBlockingStub getMyArrayIntStub() {
        ManagedChannel channel = ManagedChannelBuilder
                .forTarget(myAddress)
                .usePlaintext()
                .keepAliveWithoutCalls(true)
                .build();

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayIntStub =
                ArrayIntServiceGrpc.newBlockingStub(channel);

        return arrayIntStub;
    }

    public static List<ArrayIntServiceGrpc.ArrayIntServiceBlockingStub> createArrayIntStubs() {
        List<ArrayIntServiceGrpc.ArrayIntServiceBlockingStub> arrayIntStubs = new ArrayList<>();

        for (String neighbour : neighbours) {
            ManagedChannel channel = ManagedChannelBuilder
                    .forTarget(neighbour)
                    .usePlaintext()
                    .keepAliveWithoutCalls(true)
                    .build();

            ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayIntStub =
                    ArrayIntServiceGrpc.newBlockingStub(channel);

            arrayIntStubs.add(arrayIntStub);
        }

        return arrayIntStubs;
    }

    public static String getMyAddress() {
        return myAddress;
    }

    public static void setMyAddress(String myAddress) {
        NodeStructure.myAddress = myAddress;
    }

    public static CopyOnWriteArrayList<String> getNeighbours() {
        return neighbours;
    }

    public static void setNeighbours(CopyOnWriteArrayList<String> neighbours) {
        NodeStructure.neighbours = neighbours;
    }

    public static CopyOnWriteArrayList<Integer> getSharedVariable() {
        return sharedVariable;
    }

    public static void setSharedVariable(CopyOnWriteArrayList<Integer> sharedVariable) {
        NodeStructure.sharedVariable = sharedVariable;
    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static int getDelayMilliseconds() {
        return delayMilliseconds;
    }

    public static void setDelayMilliseconds(int delayMilliseconds) {
        NodeStructure.delayMilliseconds = delayMilliseconds;
    }
}
