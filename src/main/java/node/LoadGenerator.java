package node;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;

import java.util.concurrent.CopyOnWriteArrayList;

public class LoadGenerator {
    private boolean isActive;

    public void start() {
        isActive = true;
        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub myArrayIntStub = NodeStructure.getMyArrayIntStub();

        Thread threadMain = new Thread(() -> {
            while (isActive) {
                GracefulThread thread = null;
                CopyOnWriteArrayList<String> neighbours = NodeStructure.getNeighbours();
                boolean hasNeighbours = neighbours.size() > 0;

                if (hasNeighbours) {
                    thread = new GracefulThread(myArrayIntStub);
                    thread.start();
                }

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (hasNeighbours) {
                    thread.gracefulStop();
                }
            }
        });

        threadMain.start();
    }

    public void stop() {
        isActive = false;
    }
}
