package node;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;
import cz.cvut.fel.dsv.Services;

import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

public class GracefulThread extends Thread {

    volatile boolean finished = false;
    private final ArrayIntServiceGrpc.ArrayIntServiceBlockingStub stub;
    private static final Logger LOGGER = Logger.getLogger(GracefulThread.class.getName());

    public GracefulThread(ArrayIntServiceGrpc.ArrayIntServiceBlockingStub stub) {
        this.stub = stub;
    }

    public void gracefulStop() {
        finished = true;
    }

    public void run() {
        while (!finished) {
            try {
                int uuid = UUID.randomUUID().hashCode();
                Random rnd = new Random(uuid);

                int rndIndex = (int) (rnd.nextDouble() * 100);
                int rndPayload = (int) (rnd.nextDouble() * 100);
                int rndSleep = (int) (50 + rnd.nextDouble() * 100);

                Services.SetDataRequest setRequest =
                        Services.SetDataRequest.newBuilder()
                                .setIndex(rndIndex)
                                .setPayload(rndPayload)
                                .build();
                Services.SetDataResponse dataResponse = stub.setData(setRequest);

                Thread.sleep(rndSleep);
            } catch (Exception e) {
                e.printStackTrace();

                System.out.println(e.getMessage());

                System.out.println("ONE OF THE NODES HAS BEEN DISCONNECTED!");
                LOGGER.warning("ONE OF THE NODES HAS BEEN DISCONNECTED!");

                finished = true;

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
