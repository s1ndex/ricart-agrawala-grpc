package node;

import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

public class Blocker implements Comparable<Blocker> {
    private final long tick;

    private final ReentrantLock mutex;

    public Blocker(long tick) {
        this.tick = tick;
        this.mutex = new ReentrantLock();
    }

    public void lock() {
        mutex.lock();
    }

    public void unlock() {
        mutex.unlock();
    }

    public long getTick() {
        return tick;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Blocker blocker = (Blocker) o;
        return tick == blocker.tick;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tick);
    }

    @Override
    public int compareTo(Blocker o) {
        var r = this.tick - o.tick;
        if (r > 0) {
            return 1;
        } else if (r == 0) {
            return 0;
        } else {
            return -1;
        }
    }
}
