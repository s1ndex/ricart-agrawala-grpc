package utils;

import org.bouncycastle.util.Strings;

public class Utils {
    public static String getIpAddress(String addressPort) {
        return Strings.split(addressPort, ':')[0];
    }

    public static int getPort(String addressPort) {
        return Integer.parseInt(Strings.split(addressPort, ':')[1]);
    }
}
