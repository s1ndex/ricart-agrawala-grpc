package utils;

import server.ServerApp;

import java.io.IOException;
import java.util.logging.LogManager;

public class LoggerInit {
    public static void init() {
        try {
            LogManager
                    .getLogManager()
                    .readConfiguration(ServerApp.class.getResourceAsStream("/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e);
        }
    }
}
