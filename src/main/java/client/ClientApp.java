package client;

import cz.cvut.fel.dsv.ArrayIntServiceGrpc;
import cz.cvut.fel.dsv.Services;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class ClientApp {
    public static void main(String[] args) {

//        scenario1();
        scenario2();
//        scenario3();
//        scenario2_dos();
    }

    private static void scenario1() {
        ManagedChannel channel1 = ManagedChannelBuilder
                .forTarget("localhost:8081")
                .usePlaintext()
                .build();

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub =
                ArrayIntServiceGrpc.newBlockingStub(channel1);

        Services.SetDataRequest setRequest1 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(1)
                        .setPayload(3)
                        .build();

        Services.SetDataRequest setRequest2 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(2)
                        .setPayload(6)
                        .build();

        Thread t1 = new Thread(() -> {
            Services.SetDataResponse setResponse1 = arrayStub.setData(setRequest1);
        });
        Thread t2 = new Thread(() -> {
            Services.SetDataResponse setResponse2 = arrayStub.setData(setRequest2);
        });
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        channel1.shutdownNow();
    }

    private static void scenario2() {
        ManagedChannel channel1 = ManagedChannelBuilder
                .forTarget("localhost:8081")
                .usePlaintext()
                .build();
        ManagedChannel channel2 = ManagedChannelBuilder
                .forTarget("localhost:8083")
                .usePlaintext()
                .build();
        ManagedChannel channel3 = ManagedChannelBuilder
                .forTarget("localhost:8084")
                .usePlaintext()
                .build();

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub1 =
                ArrayIntServiceGrpc.newBlockingStub(channel1);

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub2 =
                ArrayIntServiceGrpc.newBlockingStub(channel2);

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub3 =
                ArrayIntServiceGrpc.newBlockingStub(channel3);

        Services.SetDataRequest setRequest1 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(2)
                        .setPayload(0)
                        .build();

        Services.SetDataRequest setRequest2 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(2)
                        .setPayload(1)
                        .build();

        Services.SetDataRequest setRequest3 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(2)
                        .setPayload(2)
                        .build();

        Thread t1 = new Thread(() -> {
            Services.SetDataResponse setResponse1 = arrayStub1.setData(setRequest1);
        });
        Thread t2 = new Thread(() -> {
            Services.SetDataResponse setResponse2 = arrayStub2.setData(setRequest2);
        });
        Thread t3 = new Thread(() -> {
            Services.SetDataResponse setResponse3 = arrayStub3.setData(setRequest3);
        });

        try {
            t1.start();
            Thread.sleep(100);
            t2.start();
            Thread.sleep(100);
            t3.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        channel1.shutdownNow();
        channel2.shutdownNow();
        channel3.shutdownNow();
    }

    private static void scenario2_dos() {
        ManagedChannel channel1 = ManagedChannelBuilder
                .forTarget("localhost:8081")
                .usePlaintext()
                .build();
        ManagedChannel channel2 = ManagedChannelBuilder
                .forTarget("localhost:8082")
                .usePlaintext()
                .build();

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub1 =
                ArrayIntServiceGrpc.newBlockingStub(channel1);

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub2 =
                ArrayIntServiceGrpc.newBlockingStub(channel2);

        Thread t1 = new Thread(() -> {
            for (var i = 0; i < 50; i++) {
                Services.SetDataRequest setRequest =
                        Services.SetDataRequest.newBuilder()
                                .setIndex(i)
                                .setPayload(i)
                                .build();
                Services.SetDataResponse setResponse1 = arrayStub1.setData(setRequest);
            }
        });
        Thread t2 = new Thread(() -> {
            for (var i = 50; i < 100; i++) {
                Services.SetDataRequest setRequest =
                        Services.SetDataRequest.newBuilder()
                                .setIndex(i)
                                .setPayload(i)
                                .build();
                Services.SetDataResponse setResponse2 = arrayStub2.setData(setRequest);
            }
        });
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        channel1.shutdownNow();
        channel2.shutdownNow();
    }

    // for debug
    private static void scenario3() {
        ManagedChannel channel1 = ManagedChannelBuilder
                .forTarget("localhost:8081")
                .usePlaintext()
                .build();

        ArrayIntServiceGrpc.ArrayIntServiceBlockingStub arrayStub =
                ArrayIntServiceGrpc.newBlockingStub(channel1);

        Services.SetDataRequest setRequest1 =
                Services.SetDataRequest.newBuilder()
                        .setIndex(1)
                        .setPayload(3)
                        .build();

        Thread t1 = new Thread(() -> {
            Services.SetDataResponse setResponse1 = arrayStub.setData(setRequest1);
        });

        t1.start();

        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        channel1.shutdownNow();
    }
}
