package client;

import node.Blocker;

public class SyncMain {
    public static void main(String[] args) {
        var b = new Blocker(1);
        Thread t1 = new Thread(() -> {

            System.out.println("T1 b");
            b.lock();
            System.out.println("T1 m");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            b.unlock();
            System.out.println("T1 e");
        });

        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("T2 b");
            b.lock();
            System.out.println("T2 m");
            b.unlock();
            System.out.println("T2 e");
        });

        t1.start();
        t2.start();
    }
}
