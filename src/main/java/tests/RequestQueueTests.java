package tests;

import node.Blocker;
import node.RequestQueue;
import org.junit.Assert;
import org.junit.Test;

public class RequestQueueTests {
    @Test
    public void Test1() {
        var requestQueue = new RequestQueue();
        Thread t1 = new Thread(() -> {
            for (var i = 0; i < 10; i += 2) {
                var b = new Blocker(i);
                var res = requestQueue.pushAndGetPrevious(b);
                if (res.isPresent()) {
                    Assert.assertTrue(b.getTick() > res.get().getTick());
                }
            }
        });

        Thread t2 = new Thread(() -> {
            for (var i = 1; i < 10; i += 2) {
                var b = new Blocker(i);
                var res = requestQueue.pushAndGetPrevious(b);
                if (res.isPresent()) {
                    Assert.assertTrue(b.getTick() > res.get().getTick());
                }
            }
        });

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
